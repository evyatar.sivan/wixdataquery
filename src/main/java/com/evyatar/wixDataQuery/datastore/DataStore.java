package com.evyatar.wixDataQuery.datastore;

import java.util.stream.Stream;

public interface DataStore<T> {
    Stream<T> stream();

    T add(T t);
}
