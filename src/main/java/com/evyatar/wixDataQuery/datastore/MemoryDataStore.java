package com.evyatar.wixDataQuery.datastore;

import com.evyatar.wixDataQuery.models.Entity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.stream.Stream;

@Component
public class MemoryDataStore<T extends Entity> extends HashMap<String, T> implements DataStore<T> {

    public MemoryDataStore() {
        super();
    }

    @Override
    public Stream<T> stream() {
        return this.values().stream();
    }

    @Override
    public T add(T t) {
        return this.put(t.getId(), t);
    }
}
