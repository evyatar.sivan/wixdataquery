package com.evyatar.wixDataQuery.logic.queryparser;

public class ParserSpecialCharacters {
    public static final char OPEN_PARENTHESES = '(';
    public static final char CLOSE_PARENTHESES = ')';
    public static final char SEPARATOR = ',';
    public static final char QUOTES = '"';
    public static final char BACKSLASH = '\\';
}
