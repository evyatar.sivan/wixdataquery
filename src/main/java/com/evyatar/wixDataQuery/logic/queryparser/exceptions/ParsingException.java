package com.evyatar.wixDataQuery.logic.queryparser.exceptions;

public class ParsingException extends Exception {
    public ParsingException(String message) {
        super(message);
    }
}