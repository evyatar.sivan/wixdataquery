package com.evyatar.wixDataQuery.logic.queryparser;

import com.evyatar.wixDataQuery.logic.queryparser.exceptions.ParsingException;

public interface QueryParser<T> {
    T parse(String query) throws ParsingException;
}
