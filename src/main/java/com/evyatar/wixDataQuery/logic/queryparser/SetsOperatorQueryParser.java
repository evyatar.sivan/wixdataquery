package com.evyatar.wixDataQuery.logic.queryparser;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operands.OperandFactory;
import com.evyatar.wixDataQuery.logic.operands.OperandNames;
import com.evyatar.wixDataQuery.logic.operands.exceptions.UnknownOperandException;
import com.evyatar.wixDataQuery.logic.operators.OperatorName;
import com.evyatar.wixDataQuery.logic.operators.SetsOperator;
import com.evyatar.wixDataQuery.logic.operators.SetsOperatorFactory;
import com.evyatar.wixDataQuery.logic.queryparser.exceptions.ParsingException;
import com.evyatar.wixDataQuery.logic.querytree.QueryTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.BACKSLASH;
import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.CLOSE_PARENTHESES;
import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.OPEN_PARENTHESES;
import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.QUOTES;
import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.SEPARATOR;
import static java.lang.String.format;
import static java.util.Objects.isNull;

@Component
public class SetsOperatorQueryParser implements QueryParser<QueryTree<SetsOperator, Operand>> {

    @Autowired
    private SetsOperatorFactory operatorFactory;
    @Autowired
    private OperandFactory operandFactory;

    @Override
    public QueryTree<SetsOperator, Operand> parse(String query) throws ParsingException {
        if (isNull(query) || query.isBlank())
            return null;
        assertBalancedParentheses(query);

        query = query.trim();

        OperatorName operatorName = extractOperatorName(query);
        SetsOperator operator = operatorFactory.getOperator(operatorName);

        QueryTree<SetsOperator, Operand> tree = new QueryTree<>();
        tree.setOperator(operator);

        if (!operator.isNestedOperator()) {
            tree.setOperand(parseLeafOperand(extractOperandQuery(query)));
        } else {
            query = extractOperands(query);
            int separatorIdx = findSeparatorIdx(query);
            String leftOperandQuery = extractLeftOperand(query, separatorIdx);
            String rightOperandQuery = extractRightOperand(query, separatorIdx);
            tree.setLeft(parse(leftOperandQuery));
            tree.setRight(parse(rightOperandQuery));
        }

        return tree;
    }

    private void assertBalancedParentheses(String query) throws ParsingException {
        int parenthesesCount = 0;
        boolean inQuotes = false;
        boolean isBackslash = false;

        for (int i = 0; i < query.length(); i++) {
            if (parenthesesCount < 0) {
                throwUnbalancedParenthesesException(query);
            }

            char c = query.charAt(i);

            if (inQuotes) {
                if (c == QUOTES && !isBackslash) {
                    inQuotes = !inQuotes;
                }
                if (c == BACKSLASH && !isBackslash) {
                    isBackslash = true;
                } else {
                    isBackslash = false;
                }
                continue;
            }

            switch (c) {
                case OPEN_PARENTHESES:
                    parenthesesCount++;
                    break;
                case CLOSE_PARENTHESES:
                    parenthesesCount--;
                    break;
                case QUOTES:
                    inQuotes = !inQuotes;
                    break;
            }
        }
        if (parenthesesCount != 0) {
            throwUnbalancedParenthesesException(query);
        }
    }

    private void throwUnbalancedParenthesesException(String query) throws ParsingException {
        throw new ParsingException(format("Invalid Query: %s - Not balanced parentheses", query));
    }

    private OperatorName extractOperatorName(String query) throws ParsingException {
        query = query.trim();
        int openParenthesesIdx = findParenthesesIdx(query);
        String operatorString = query.substring(0, openParenthesesIdx).trim();
        try {
            return OperatorName.valueOf(operatorString);
        } catch (IllegalArgumentException e) {
            throw new ParsingException(format("Found illegal operation: %s", operatorString));
        }
    }

    /**
     * @param query - a query assumed to be in the following structure: " OPERATOR ( .... ) ... "
     * @return - the string inside the parentheses
     * @throws ParsingException - if query is not in the assumed structure
     */
    private String extractOperandQuery(String query) throws ParsingException {
        query = query.trim();
        try {
            int openParenthesesIdx = findParenthesesIdx(query);
            String operandQuery = query.substring(openParenthesesIdx).trim();
            int closingOperandParenthesesIdx = findFirstCharByLevel(operandQuery, CLOSE_PARENTHESES, 1);
            operandQuery = operandQuery.substring(1, closingOperandParenthesesIdx);

            return operandQuery;
        } catch (StringIndexOutOfBoundsException e) {
            throw new ParsingException(format("Invalid Query: %s - cannot extract operand", query));
        }
    }

    private int findSeparatorIdx(String query) {
        return findFirstCharByLevel(query, SEPARATOR, 1);
    }

    /**
     * This function runs through a string
     * searches for the correct char toFind according to parentheses calculation
     * ignoring chars inside quotes
     *
     * @param query - a query string
     * @return the index of toFind char
     */
    private int findFirstCharByLevel(String query, char toFind, int level) {
        int parenthesesCount = 0;
        boolean inQuotes = false;
        boolean isBackslash = false;
        for (int i = 0; i < query.length(); i++) {
            char c = query.charAt(i);

            if (inQuotes) {
                if (c == QUOTES && !isBackslash) {
                    inQuotes = !inQuotes;
                }
                if (c == BACKSLASH && !isBackslash) {
                    isBackslash = true;
                } else {
                    isBackslash = false;
                }
                continue;
            }

            if (c == toFind && parenthesesCount == level) {
                return i;
            }

            switch (c) {
                case OPEN_PARENTHESES:
                    parenthesesCount++;
                    break;
                case CLOSE_PARENTHESES:
                    parenthesesCount--;
                    break;
                case QUOTES:
                    inQuotes = !inQuotes;
                    break;
            }
        }
        return -1;
    }

    /**
     * This function assumes the following structure
     * " ( OPERATOR ( OPERAND_A(..) , OPERAND_B(..) ) ) "
     * and returns the following structure
     * "( OPERAND_A(..) , OPERAND_B(..) )"
     *
     * @param query - a query string
     * @return the operands in the parentheses
     */
    private String extractOperands(String query) throws ParsingException {
        query = query.trim();
        int startOfOpernadsIdx = findParenthesesIdx(query);
        int endOfOperandsIdx = findFirstCharByLevel(query, CLOSE_PARENTHESES, 1);
        try {
            query = query.substring(startOfOpernadsIdx, endOfOperandsIdx + 1).trim();
        } catch (StringIndexOutOfBoundsException e) {
            throw new ParsingException(format("Invalid Query: %s - cannot extract operands", query));
        }
        assertParenthesesEnclosed(query);

        return query;
    }

    private String extractLeftOperand(String query, int seperatorIdx) throws ParsingException {
        query = query.trim();
        assertParenthesesEnclosed(query);
        if (seperatorIdx == -1) {
            query = query.substring(1, query.length() - 1);
        } else {
            query = query.substring(1, seperatorIdx);
        }
        assertNoExtraTextLeftOperand(query);

        return query;
    }

    private String extractRightOperand(String query, int seperatorIdx) throws ParsingException {
        assertParenthesesEnclosed(query);
        if (seperatorIdx == -1) {
            return null;
        }
        query = query.substring(seperatorIdx + 1, query.length() - 1);
        assertNoExtraTextRightOperand(query);
        int operandClosingParenthesesIdx = findFirstCharByLevel(query, CLOSE_PARENTHESES, 1);
        query = query.substring(0, operandClosingParenthesesIdx + 1);

        return query;
    }

    private void assertParenthesesEnclosed(String query) throws ParsingException {
        if (query.length() <= 1
                || query.charAt(0) != OPEN_PARENTHESES
                || query.charAt(query.length() - 1) <= 1) {
            throw new ParsingException(format("Invalid Query: %s - must be enclosed by parentheses", query));
        }
    }

    private void assertNoExtraTextLeftOperand(String query) throws ParsingException {
        query = query.trim();
        int closingParenthesesIdx = findFirstCharByLevel(query, CLOSE_PARENTHESES, 1);
        if (closingParenthesesIdx != query.length() - 1) {
            throw new ParsingException("Found extra text");
        }
    }

    private void assertNoExtraTextRightOperand(String query) throws ParsingException {
        query = query.trim();
        int closingParenthesesIdx = findFirstCharByLevel(query, CLOSE_PARENTHESES, 1);
        int nextParenthesesIdx = findFirstCharByLevel(query.substring(closingParenthesesIdx), CLOSE_PARENTHESES, -2);

        int searchTextIdx = nextParenthesesIdx == -1
                ? query.length()
                : nextParenthesesIdx;
        if (!query.substring(closingParenthesesIdx + 1, searchTextIdx).isBlank()) {
            throw new ParsingException("Found extra text");
        }
    }

    private Operand parseLeafOperand(String operandQuery) throws ParsingException, UnknownOperandException {
        try {
            Operand operand = operandFactory.createOperand(
                    OperandNames.valueOf(operandQuery.split(",")[0].trim()),
                    operandQuery.split(",")[1].trim()
            );
            if (operand.getType() == String.class) {
                operand.setValue(extractStringValue(operandQuery.split(",")[1].trim()));
            }
            return operand;
        } catch (UnknownOperandException | ParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ParsingException(format("Illegal Operand: %s", operandQuery));
        }
    }

    private String extractStringValue(String value) throws ParsingException {
        if (value == null || value.length() < 2 || value.charAt(0) != QUOTES || value.charAt(value.length() - 1) != QUOTES) {
            throw new ParsingException("String values should be enclosed with quotes");
        }
        StringBuilder result = new StringBuilder();
        boolean isBackslash = false;
        for (int i = 1; i < value.length() - 1; i++) {
            char c = value.charAt(i);
            if (c == QUOTES && !isBackslash) {
                if (i != value.length() - 1) {
                    throw new ParsingException(String.format("Found unexpected quotes in expression: %s", value));
                }
            } else if (c == BACKSLASH) {
                isBackslash = !isBackslash;
                continue;
            } else {
                isBackslash = false;
            }
            result.append(c);
        }

        return result.toString();
    }

    private int findParenthesesIdx(String query) throws ParsingException {
        int idx = query.indexOf(OPEN_PARENTHESES);
        if (idx == -1) {
            throw new ParsingException("Missing Parentheses");
        }

        return idx;
    }
}
