package com.evyatar.wixDataQuery.logic.processor;

import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;

import java.util.Set;

public interface QueryProcessor<T extends RetrievableFieldsByName, QUERY_DATA_STRUCTURE> {
    Set<T> process(QUERY_DATA_STRUCTURE queryDataStructure, Set<T> datastream);
}