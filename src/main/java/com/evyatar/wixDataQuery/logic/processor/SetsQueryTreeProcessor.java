package com.evyatar.wixDataQuery.logic.processor;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.SetsOperator;
import com.evyatar.wixDataQuery.logic.querytree.QueryTree;
import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class SetsQueryTreeProcessor<T extends RetrievableFieldsByName> implements QueryProcessor<T, QueryTree<SetsOperator, Operand>> {

    @Override
    public Set<T> process(QueryTree<SetsOperator, Operand> queryTree, Set<T> dataStore) {
        if (queryTree == null) {
            return dataStore;
        }

        SetsOperator operator = queryTree.getOperator();
        Operand operand = queryTree.getOperand();

        if (queryTree.isLeaf()) {
            return operator.apply(operand, dataStore);
        } else {
            Set<T> leftTreeStream = process(queryTree.getLeft(), dataStore);
            Set<T> rightTreeStream = process(queryTree.getRight(), dataStore);
            return operator.apply(leftTreeStream, rightTreeStream);
        }
    }
}
