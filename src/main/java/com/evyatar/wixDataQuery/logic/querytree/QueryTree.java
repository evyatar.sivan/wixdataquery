package com.evyatar.wixDataQuery.logic.querytree;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.Operator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.util.Objects.isNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class QueryTree<T extends Operator, U extends Operand> {
    private T operator;
    private U operand;
    private QueryTree<T, U> left;
    private QueryTree<T, U> right;

    public boolean isLeaf() {
        return isNull(left) && isNull(right);
    }
}
