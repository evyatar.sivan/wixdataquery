package com.evyatar.wixDataQuery.logic.operators.exceptions;

public class IllegalOperatorException extends RuntimeException {
    public IllegalOperatorException(String message) {
        super(message);
    }
}
