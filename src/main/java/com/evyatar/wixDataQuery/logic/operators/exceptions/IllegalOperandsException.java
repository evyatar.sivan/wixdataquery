package com.evyatar.wixDataQuery.logic.operators.exceptions;

import com.evyatar.wixDataQuery.logic.operators.OperatorName;

public class IllegalOperandsException extends OperatorApplyingException {
    public IllegalOperandsException(String message) {
        super(message);
    }

    public static <T> IllegalOperandsException illegalOperandsException(OperatorName operation, String operandField, Object datumField) {
        return new IllegalOperandsException(
                String.format("Could not apply %s to: %s, %s", operation, operandField, datumField)
        );
    }
}
