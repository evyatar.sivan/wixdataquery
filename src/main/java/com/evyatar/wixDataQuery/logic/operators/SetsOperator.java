package com.evyatar.wixDataQuery.logic.operators;

import java.util.Set;

public abstract class SetsOperator<U, T> implements Operator<U, Set<T>, Set<T>> {
    public abstract Set<T> apply(U u, Set<T> s);

    public abstract boolean isNestedOperator();
}
