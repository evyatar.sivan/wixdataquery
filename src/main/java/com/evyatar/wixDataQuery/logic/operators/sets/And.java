package com.evyatar.wixDataQuery.logic.operators.sets;

import com.evyatar.wixDataQuery.logic.operators.SetsOperator;
import com.google.common.collect.Sets;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class And<T> extends SetsOperator<Set<T>, T> {
    @Override
    public Set<T> apply(Set<T> s1, Set<T> s2) {
        return Sets.intersection(s1, s2);
    }

    @Override
    public boolean isNestedOperator() {
        return true;
    }
}
