package com.evyatar.wixDataQuery.logic.operators.compare;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.OperatorName;
import com.evyatar.wixDataQuery.logic.operators.SetsOperator;
import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

import static com.evyatar.wixDataQuery.logic.operators.exceptions.IllegalOperandsException.illegalOperandsException;
import static com.evyatar.wixDataQuery.logic.operators.exceptions.OperatorApplyingException.missingFieldException;

@Component
public class LowerThan<T extends RetrievableFieldsByName> extends SetsOperator<Operand, T> {
    public Boolean lowerThan(Operand<Integer> operand, RetrievableFieldsByName datum) {
        double fieldValue;

        try {
            fieldValue = (Integer) datum.getValueByField(operand.getName());
        } catch (NullPointerException e) {
            return false;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw missingFieldException(operand.getName(), datum);
        }

        try {
            return fieldValue < operand.getValue();
        } catch (ClassCastException | NumberFormatException e) {
            throw illegalOperandsException(OperatorName.LESS_THAN, operand.getName(), fieldValue);
        }
    }

    @Override
    public Set<T> apply(Operand operand, Set<T> s) {
        return s.stream().filter(t -> lowerThan(operand, t)).collect(Collectors.toSet());
    }

    @Override
    public boolean isNestedOperator() {
        return false;
    }
}
