package com.evyatar.wixDataQuery.logic.operators;

import com.evyatar.wixDataQuery.logic.operators.compare.Equals;
import com.evyatar.wixDataQuery.logic.operators.compare.GreaterThan;
import com.evyatar.wixDataQuery.logic.operators.compare.LowerThan;
import com.evyatar.wixDataQuery.logic.operators.exceptions.IllegalOperatorException;
import com.evyatar.wixDataQuery.logic.operators.sets.And;
import com.evyatar.wixDataQuery.logic.operators.sets.Not;
import com.evyatar.wixDataQuery.logic.operators.sets.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SetsOperatorFactory<T> implements OperatorFactory<SetsOperator> {
    @Autowired
    private Equals equals;
    @Autowired
    private GreaterThan gt;
    @Autowired
    private LowerThan lt;
    @Autowired
    private Or<T> or;
    @Autowired
    private And<T> and;
    @Autowired
    private Not<T> not;

    public SetsOperator getOperator(OperatorName operatorName) {
        switch (operatorName) {
            case EQUAL:
                return equals;
            case LESS_THAN:
                return lt;
            case GREATER_THAN:
                return gt;
            case OR:
                return or;
            case AND:
                return and;
            case NOT:
                return not;
            default:
                throw new IllegalOperatorException(String.format("Unknown operator: %s", operatorName));
        }
    }
}
