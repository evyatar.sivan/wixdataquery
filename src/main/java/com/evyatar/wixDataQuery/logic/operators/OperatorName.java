package com.evyatar.wixDataQuery.logic.operators;

public enum OperatorName {
    NOT,
    AND,
    OR,
    EQUAL,
    GREATER_THAN,
    LESS_THAN
}
