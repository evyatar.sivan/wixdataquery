package com.evyatar.wixDataQuery.logic.operators.exceptions;

public class OperatorApplyingException extends RuntimeException {
    public OperatorApplyingException(String message) {
        super(message);
    }

    public static <T> OperatorApplyingException missingFieldException(String field, T datum) {
        return new OperatorApplyingException(
                String.format("Field %s does not exists for element %s", field, datum)
        );
    }
}
