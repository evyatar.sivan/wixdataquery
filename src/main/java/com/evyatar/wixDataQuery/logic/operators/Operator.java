package com.evyatar.wixDataQuery.logic.operators;

public interface Operator<O1, O2, R> {
    R apply(O1 o1, O2 o2);

    boolean isNestedOperator();
}
