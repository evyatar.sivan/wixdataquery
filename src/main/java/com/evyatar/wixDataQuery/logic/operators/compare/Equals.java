package com.evyatar.wixDataQuery.logic.operators.compare;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.SetsOperator;
import com.evyatar.wixDataQuery.logic.operators.exceptions.OperatorApplyingException;
import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

import static com.evyatar.wixDataQuery.logic.operators.exceptions.OperatorApplyingException.missingFieldException;
import static java.lang.String.format;

@Component
public class Equals<T extends RetrievableFieldsByName> extends SetsOperator<Operand, T> {

    private Boolean equals(Operand operand, T datum) {
        try {
            return operand.getType()
                    .cast(datum.getValueByField(operand.getName()))
                    .equals(operand.getValue());
        } catch (NullPointerException e) {
            return false;
        } catch (ClassCastException e) {
            throw new OperatorApplyingException(format("field %s is not of type: %s", operand.getName(), operand.getType().toString()));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw missingFieldException(operand.getName(), datum);
        }
    }

    @Override
    public Set<T> apply(Operand operand, Set<T> s) {
        return s.stream().filter(t -> equals(operand, t)).collect(Collectors.toSet());
    }

    @Override
    public boolean isNestedOperator() {
        return false;
    }
}
