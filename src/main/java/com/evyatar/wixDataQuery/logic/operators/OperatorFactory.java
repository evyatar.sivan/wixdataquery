package com.evyatar.wixDataQuery.logic.operators;

public interface OperatorFactory<O> {
    O getOperator(OperatorName operatorName);
}
