package com.evyatar.wixDataQuery.logic.operands;

public class Views extends Operand<Integer> {

    public Views(String value) {
        this.value = Integer.parseInt(value);
    }

    @Override
    public String getName() {
        return "views";
    }

    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }
}
