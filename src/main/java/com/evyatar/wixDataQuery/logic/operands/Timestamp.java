package com.evyatar.wixDataQuery.logic.operands;

public class Timestamp extends Operand<Integer> {

    public Timestamp(String value) {
        this.value = Integer.parseInt(value);
    }

    @Override
    public String getName() {
        return "timestamp";
    }

    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }
}
