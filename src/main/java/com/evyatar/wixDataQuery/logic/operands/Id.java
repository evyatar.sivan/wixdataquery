package com.evyatar.wixDataQuery.logic.operands;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Id extends Operand<String> {
    public Id(String value) {
        this.value = value;
    }

    @Override
    public String getName() {
        return "id";
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }
}
