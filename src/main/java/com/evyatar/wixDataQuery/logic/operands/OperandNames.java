package com.evyatar.wixDataQuery.logic.operands;

public enum OperandNames {
    id,
    title,
    content,
    views,
    timestamp
}
