package com.evyatar.wixDataQuery.logic.operands;

import com.evyatar.wixDataQuery.logic.operands.exceptions.UnknownOperandException;
import org.springframework.stereotype.Component;

@Component
public class OperandFactory {
    public Operand createOperand(OperandNames name, String value) throws UnknownOperandException {
        switch (name) {
            case id:
                return new Id(value);
            case title:
                return new Title(value);
            case content:
                return new Content(value);
            case views:
                return new Views(value);
            case timestamp:
                return new Timestamp(value);
            default:
                throw new UnknownOperandException(String.format("Unknown Operand: %s ", name));
        }
    }
}
