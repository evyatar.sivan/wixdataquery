package com.evyatar.wixDataQuery.logic.operands;

public class Title extends Operand<String> {

    public Title(String value) {
        this.value = value;
    }

    @Override
    public String getName() {
        return "title";
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }
}
