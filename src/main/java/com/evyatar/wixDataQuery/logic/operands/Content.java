package com.evyatar.wixDataQuery.logic.operands;

public class Content extends Operand<String> {

    public Content(String value) {
        this.value = value;
    }

    @Override
    public String getName() {
        return "content";
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }
}
