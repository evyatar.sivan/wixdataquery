package com.evyatar.wixDataQuery.logic.operands.exceptions;

public class UnknownOperandException extends RuntimeException {
    public UnknownOperandException(String message) {
        super(message);
    }
}
