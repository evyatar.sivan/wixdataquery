package com.evyatar.wixDataQuery.logic.operands;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public abstract class Operand<T> {
    protected T value;

    public abstract String getName();

    public abstract Class<T> getType();
}
