package com.evyatar.wixDataQuery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WixDataQueryApplication {

    public static void main(String[] args) {
        SpringApplication.run(WixDataQueryApplication.class, args);
    }
}
