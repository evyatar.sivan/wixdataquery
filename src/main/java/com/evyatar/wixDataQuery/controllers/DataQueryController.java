package com.evyatar.wixDataQuery.controllers;

import com.evyatar.wixDataQuery.controllers.errors.ErrorData;
import com.evyatar.wixDataQuery.controllers.output.EmptyJSONOutput;
import com.evyatar.wixDataQuery.logic.queryparser.exceptions.ParsingException;
import com.evyatar.wixDataQuery.models.Data;
import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;
import com.evyatar.wixDataQuery.services.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Set;

@RequestMapping(DataQueryController.ENDPOINT)
@RestController
public class DataQueryController {
    public static final String ENDPOINT = "/store";

    @Autowired
    private DataService<RetrievableFieldsByName> dataService;

    @GetMapping
    public Set<RetrievableFieldsByName> getDataByQuery(@RequestParam String query) throws ParsingException {
        query = URLDecoder.decode(query, StandardCharsets.UTF_8);
        return dataService.getDataByQuery(query);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmptyJSONOutput> storeData(@RequestBody Data data) {
        dataService.storeData(data);
        return new ResponseEntity<>(new EmptyJSONOutput(), HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ErrorData handleError(HttpServletRequest req, Exception ex) {
        return ErrorData.builder()
                .url(req.getRequestURL().toString())
                .message(ex.getMessage())
                .build();
    }
}