package com.evyatar.wixDataQuery.controllers.errors;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class ErrorData {
    String url;
    String message;
}
