package com.evyatar.wixDataQuery.services;

import com.evyatar.wixDataQuery.datastore.DataStore;
import com.evyatar.wixDataQuery.logic.processor.QueryProcessor;
import com.evyatar.wixDataQuery.logic.queryparser.QueryParser;
import com.evyatar.wixDataQuery.logic.queryparser.exceptions.ParsingException;
import com.evyatar.wixDataQuery.logic.querytree.QueryTree;
import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service
public class DataQueryService<T extends RetrievableFieldsByName> implements DataService<T> {

    @Autowired
    private DataStore<T> dataStore;
    @Autowired
    private QueryParser<QueryTree> queryParser;
    @Autowired
    private QueryProcessor<T, QueryTree> queryProcessor;

    @Override
    public Set<T> getDataByQuery(String query) throws ParsingException {
        QueryTree queryTree = queryParser.parse(query);
        return queryProcessor.process(queryTree, dataStore.stream().collect(toSet()));
    }

    @Override
    public void storeData(T entity) {
        dataStore.add(entity);
    }
}
