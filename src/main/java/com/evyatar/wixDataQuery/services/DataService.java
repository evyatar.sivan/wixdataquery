package com.evyatar.wixDataQuery.services;

import com.evyatar.wixDataQuery.logic.queryparser.exceptions.ParsingException;
import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface DataService<T extends RetrievableFieldsByName> {
    Set<T> getDataByQuery(String query) throws ParsingException;

    void storeData(T entity);
}
