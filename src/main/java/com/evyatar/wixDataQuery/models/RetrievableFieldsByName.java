package com.evyatar.wixDataQuery.models;

public interface RetrievableFieldsByName {
    Object getValueByField(String fieldName) throws NoSuchFieldException, IllegalAccessException;
}
