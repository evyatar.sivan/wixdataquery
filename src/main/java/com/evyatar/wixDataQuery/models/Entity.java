package com.evyatar.wixDataQuery.models;

public interface Entity extends RetrievableFieldsByName {
    String getId();
}
