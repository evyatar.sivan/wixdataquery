package com.evyatar.wixDataQuery.utils.queryGenerators;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.OperatorName;

public interface OperatorQueryGenerator<T extends Operand> {
    String generateQuery(OperatorName name, T operand);

    String generateQuery(OperatorName name, String operand);

    String generateQuery(OperatorName name, String operand1, String operand2);

    String generateUnsafeQuery(String operator, String operand);

    String generateUnsafeQuery(String operator, String operand1, String operand2);
}
