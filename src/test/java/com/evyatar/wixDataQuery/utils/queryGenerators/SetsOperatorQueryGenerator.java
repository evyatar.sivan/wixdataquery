package com.evyatar.wixDataQuery.utils.queryGenerators;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.OperatorName;
import org.springframework.stereotype.Component;

import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.CLOSE_PARENTHESES;
import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.OPEN_PARENTHESES;
import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.QUOTES;
import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.SEPARATOR;
import static java.lang.String.format;

@Component
public class SetsOperatorQueryGenerator implements OperatorQueryGenerator<Operand> {
    @Override
    public String generateQuery(OperatorName name, Operand operand) {
        String operandValue = operand.getType() == String.class
                ? QUOTES + operand.getValue().toString() + QUOTES
                : operand.getValue().toString();

        return format("%s%s%s%s%s%s",
                name, OPEN_PARENTHESES, operand.getName(),
                SEPARATOR, operandValue, CLOSE_PARENTHESES);
    }

    @Override
    public String generateQuery(OperatorName name, String operand) {
        return format("%s%s%s%s",
                name, OPEN_PARENTHESES, operand, CLOSE_PARENTHESES);
    }

    @Override
    public String generateQuery(OperatorName name, String operand1, String operand2) {
        return format("%s%s%s%s%s%s",
                name, OPEN_PARENTHESES, operand1,
                SEPARATOR, operand2, CLOSE_PARENTHESES);
    }

    @Override
    public String generateUnsafeQuery(String operator, String operand) {
        return format("%s%s%s%s", operator, OPEN_PARENTHESES, operand, CLOSE_PARENTHESES);
    }

    @Override
    public String generateUnsafeQuery(String operator, String operand1, String operand2) {
        return format("%s%s%s%s%s%s",
                operator, OPEN_PARENTHESES, operand1,
                SEPARATOR, operand2, CLOSE_PARENTHESES);
    }
}
