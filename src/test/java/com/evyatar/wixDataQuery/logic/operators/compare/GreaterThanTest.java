package com.evyatar.wixDataQuery.logic.operators.compare;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.exceptions.IllegalOperandsException;
import com.evyatar.wixDataQuery.logic.operators.exceptions.OperatorApplyingException;
import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = GreaterThan.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class GreaterThanTest {

    @MockBean
    private Operand operand;
    @Autowired
    private GreaterThan<RetrievableFieldsByName> gt;

    @Test
    public void compareIntegers() throws NoSuchFieldException, IllegalAccessException {
        when(operand.getType()).thenReturn(Integer.class);
        when(operand.getName()).thenReturn("views");
        when(operand.getValue()).thenReturn(2000);

        Set<RetrievableFieldsByName> doubleFieldSet = Set.of(
                fieldName -> 3000,
                fieldName -> 2000
        );

        Set<RetrievableFieldsByName> filteredSet = gt.apply(operand, doubleFieldSet);
        assertEquals(1, filteredSet.size());
        assertFalse(filteredSet.stream().findFirst().isEmpty());
        assertEquals(3000, filteredSet.stream().findFirst().get().getValueByField("views"));
    }


    @Test(expected = IllegalOperandsException.class)
    public void illegalOperandTest() {
        when(operand.getType()).thenReturn(Integer.class);
        when(operand.getValue()).thenReturn("StringValue");

        Set<RetrievableFieldsByName> datastore = Set.of(
                fieldName -> 3000,
                fieldName -> 2000
        );

        gt.apply(operand, datastore);
    }

    @Test(expected = OperatorApplyingException.class)
    public void compareUnknownField() {
        when(operand.getType()).thenReturn(Integer.class);
        when(operand.getValue()).thenReturn(123);

        Set<RetrievableFieldsByName> set = Set.of(
                fieldName -> {
                    throw new NoSuchFieldException("");
                }
        );

        gt.apply(operand, set);
    }

    @Test
    public void compareNullReturnsFalse() throws NoSuchFieldException, IllegalAccessException {
        when(operand.getType()).thenReturn(Integer.class);
        when(operand.getName()).thenReturn("views");
        when(operand.getValue()).thenReturn(1000);

        Set<RetrievableFieldsByName> intSet = Set.of(
                fieldName -> null,
                fieldName -> 2000
        );

        Set<RetrievableFieldsByName> filteredSet = gt.apply(operand, intSet);
        assertEquals(1, filteredSet.size());
        assertFalse(filteredSet.stream().findFirst().isEmpty());
        assertEquals(2000, filteredSet.stream().findFirst().get().getValueByField("views"));
    }
}