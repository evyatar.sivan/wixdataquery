package com.evyatar.wixDataQuery.logic.operators.compare;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.exceptions.OperatorApplyingException;
import com.evyatar.wixDataQuery.models.RetrievableFieldsByName;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = Equals.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class EqualsTest {

    @MockBean
    private Operand operand;
    @Autowired
    private Equals<RetrievableFieldsByName> equals;

    @Test
    public void compareIntegers() throws NoSuchFieldException, IllegalAccessException {
        when(operand.getType()).thenReturn(Integer.class);
        when(operand.getName()).thenReturn("views");
        when(operand.getValue()).thenReturn(2000);

        Set<RetrievableFieldsByName> doubleFieldSet = Set.of(
                fieldName -> 3000,
                fieldName -> 2000
        );

        Set<RetrievableFieldsByName> filteredSet = equals.apply(operand, doubleFieldSet);
        assertEquals(1, filteredSet.size());
        assertFalse(filteredSet.stream().findFirst().isEmpty());
        assertEquals(2000, filteredSet.stream().findFirst().get().getValueByField("views"));
    }

    @Test
    public void compareStrings() throws NoSuchFieldException, IllegalAccessException {
        when(operand.getType()).thenReturn(String.class);
        when(operand.getName()).thenReturn("id");
        when(operand.getValue()).thenReturn("id1");

        Set<RetrievableFieldsByName> doubleFieldSet = Set.of(
                fieldName -> "id1",
                fieldName -> "id2"
        );

        Set<RetrievableFieldsByName> filteredSet = equals.apply(operand, doubleFieldSet);
        assertEquals(1, filteredSet.size());
        assertFalse(filteredSet.stream().findFirst().isEmpty());
        assertEquals("id1", filteredSet.stream().findFirst().get().getValueByField("id"));
    }

    @Test(expected = OperatorApplyingException.class)
    public void compareUnknownField() {
        when(operand.getType()).thenReturn(String.class);
        when(operand.getName()).thenReturn("notHere");
        when(operand.getValue()).thenReturn("123");

        Set<RetrievableFieldsByName> doubleFieldSet = Set.of(
                fieldName -> {
                    throw new NoSuchFieldException("");
                }
        );

        equals.apply(operand, doubleFieldSet);
    }

    @Test(expected = OperatorApplyingException.class)
    public void compareMismatchingTypes() {
        when(operand.getType()).thenReturn(String.class);
        when(operand.getName()).thenReturn("stringOp");
        when(operand.getValue()).thenReturn("wow");

        Set<RetrievableFieldsByName> intSet = Set.of(
                fieldName -> 1,
                fieldName -> 2
        );

        equals.apply(operand, intSet);
    }

    @Test
    public void compareNullReturnsFalse() throws NoSuchFieldException, IllegalAccessException {
        when(operand.getType()).thenReturn(String.class);
        when(operand.getName()).thenReturn("id");
        when(operand.getValue()).thenReturn("id");

        Set<RetrievableFieldsByName> intSet = Set.of(
                fieldName -> null,
                fieldName -> "id"
        );

        Set<RetrievableFieldsByName> filteredSet = equals.apply(operand, intSet);
        assertEquals(1, filteredSet.size());
        assertFalse(filteredSet.stream().findFirst().isEmpty());
        assertEquals("id", filteredSet.stream().findFirst().get().getValueByField("id"));
    }
}