package com.evyatar.wixDataQuery.logic.processor;

import com.evyatar.wixDataQuery.datastore.DataStore;
import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operators.SetsOperator;
import com.evyatar.wixDataQuery.logic.querytree.QueryTree;
import com.evyatar.wixDataQuery.models.Data;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {
        SetsQueryTreeProcessor.class,
})
@RunWith(SpringJUnit4ClassRunner.class)
public class QueryTreeProcessorTest {

    @Autowired
    private SetsQueryTreeProcessor<Data> queryTreeProcessor;
    @MockBean
    private DataStore<Data> dataStore;
    @MockBean
    private SetsOperator operator;
    @MockBean
    private Operand operand;

    @Before
    public void setUp() {
        Stream.Builder<Data> datastream = Stream.builder();
        IntStream.range(0, 10).forEach(i ->
                datastream.accept(Data.builder()
                        .id(String.valueOf(i))
                        .title("title " + i)
                        .content("content " + i)
                        .timestamp(100000 + i)
                        .views(i)
                        .build()));
        when(dataStore.stream()).thenReturn(datastream.build());
    }

    @Test
    public void nullOperatorTreeReturnsAllElementsTest() {
        Set<Data> dataSet = dataStore.stream().collect(toSet());
        Set<Data> proccessedDataSet = queryTreeProcessor.process(null, dataSet);

        assertEquals(dataSet, proccessedDataSet);
    }

    @Test
    public void leafOperatorTest() {
        Set<Data> dataSet = dataStore.stream().collect(toSet());
        QueryTree queryTree = QueryTree.builder()
                .operator(operator)
                .operand(operand)
                .build();

        queryTreeProcessor.process(queryTree, dataSet);

        verify(operator, times(1)).apply(operand, dataSet);
    }

    @Test
    public void nestedOperatorTest() {
        Set<Data> dataSet = dataStore.stream().collect(toSet());
        QueryTree queryTree = QueryTree.builder()
                .operator(operator)
                .left(QueryTree.builder()
                        .operator(operator)
                        .operand(operand)
                        .build())
                .right(QueryTree.builder()
                        .operator(operator)
                        .operand(operand)
                        .build())
                .build();

        queryTreeProcessor.process(queryTree, dataSet);

        verify(operator, times(1)).apply(anySet(), anySet());
        verify(operator, times(2)).apply(operand, dataSet);
    }
}