package com.evyatar.wixDataQuery.logic.queryparser;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.evyatar.wixDataQuery.logic")
public class SetsOperatorQueryParserTestConfiguration {
}
