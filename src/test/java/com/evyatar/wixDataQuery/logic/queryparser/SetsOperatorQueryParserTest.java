package com.evyatar.wixDataQuery.logic.queryparser;

import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operands.OperandFactory;
import com.evyatar.wixDataQuery.logic.operands.OperandNames;
import com.evyatar.wixDataQuery.logic.operators.OperatorName;
import com.evyatar.wixDataQuery.logic.operators.SetsOperator;
import com.evyatar.wixDataQuery.logic.operators.SetsOperatorFactory;
import com.evyatar.wixDataQuery.logic.queryparser.exceptions.ParsingException;
import com.evyatar.wixDataQuery.logic.querytree.QueryTree;
import com.evyatar.wixDataQuery.utils.queryGenerators.SetsOperatorQueryGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.CLOSE_PARENTHESES;
import static com.evyatar.wixDataQuery.logic.queryparser.ParserSpecialCharacters.OPEN_PARENTHESES;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ContextConfiguration(classes = {
        SetsOperatorQueryParserTestConfiguration.class,
        SetsOperatorQueryGenerator.class,
})
@RunWith(SpringJUnit4ClassRunner.class)
public class SetsOperatorQueryParserTest {

    @Autowired
    private SetsOperatorQueryParser parser;
    @Autowired
    private SetsOperatorQueryGenerator queryGenerator;
    @Autowired
    private SetsOperatorFactory setsOperatorFactory;
    @Autowired
    private OperandFactory operandFactory;

    @Test
    public void parseEmptyQueryReturnsNullTest() throws ParsingException {
        assertNull(parser.parse(""));
    }

    @Test(expected = ParsingException.class)
    public void parseUnknownOperatorTest() throws ParsingException {
        String query = queryGenerator.generateUnsafeQuery("NOTEXISTS", "views, 1");

        parser.parse(query);
    }

    @Test(expected = ParsingException.class)
    public void parseUnknownOperandTest() throws ParsingException {
        String query = queryGenerator.generateUnsafeQuery(
                OperatorName.EQUAL.toString(), "NOTEXISTS, 1");

        parser.parse(query);
    }

    @Test(expected = ParsingException.class)
    public void parseInvalidOperandExceptionTest() throws ParsingException {
        String query = queryGenerator.generateUnsafeQuery(
                OperatorName.EQUAL.toString(),
                "INVALID_OPERAND_MISSING_COMMA"
        );

        parser.parse(query);
    }

    @Test(expected = ParsingException.class)
    public void parseUnbalancedParenthesesTest() throws ParsingException {
        String query = queryGenerator.generateUnsafeQuery(
                OperatorName.EQUAL.toString(), OPEN_PARENTHESES + "views, 5"
        );

        parser.parse(query);
    }

    @Test(expected = ParsingException.class)
    public void parseNegativeUnbalancedParenthesesTest() throws ParsingException {
        String query = queryGenerator.generateUnsafeQuery(
                CLOSE_PARENTHESES + OperatorName.EQUAL.toString(), "views, 5"
        );

        parser.parse(query);
    }

    @Test(expected = ParsingException.class)
    public void parseStringOperandMissingQuotesTest() throws ParsingException {
        String query = queryGenerator.generateUnsafeQuery(
                OperatorName.EQUAL.toString(), "content, 5"
        );

        parser.parse(query);
    }

    @Test(expected = ParsingException.class)
    public void parseInvalidSomeTextInTheMiddleTest() throws ParsingException {
        String subquery1 = queryGenerator.generateQuery(OperatorName.GREATER_THAN, "views", "\"1\"");
        String subquery2 = queryGenerator.generateQuery(OperatorName.LESS_THAN, "views", "\"5\"");
        String query = queryGenerator.generateQuery(OperatorName.AND, subquery1 + " SOME_TEXT ", subquery2);

        parser.parse(query);
    }

    @Test(expected = ParsingException.class)
    public void parseInvalidSomeTextInTheRightOperandTest() throws ParsingException {
        String subquery1 = queryGenerator.generateQuery(OperatorName.EQUAL, "id", "\"1\"");
        String subquery2 = queryGenerator.generateQuery(OperatorName.EQUAL, "id", "\"5\"");
        String query = queryGenerator.generateQuery(OperatorName.OR, subquery1, subquery2 + " SOME_TEXT ");

        parser.parse(query);
    }

    @Test
    public void parseQueryWithSpacesTest() throws ParsingException {
        String query = queryGenerator.generateQuery(OperatorName.EQUAL, "id", "\"5\"");

        QueryTree expectedTree = QueryTree.builder()
                .operator(setsOperatorFactory.getOperator(OperatorName.EQUAL))
                .operand(operandFactory.createOperand(OperandNames.id, "5"))
                .build();

        QueryTree queryTree = parser.parse(query);
        assertEquals(expectedTree, queryTree);
    }

    @Test
    public void parseQueryWithSpecialCharsInTextTest() throws ParsingException {
        String text = "\"this is \\\" ) ( a valid text )\"";
        String subquery1 = queryGenerator.generateQuery(
                OperatorName.EQUAL, OperandNames.content.toString(), text);
        String subquery2 = queryGenerator.generateQuery(
                OperatorName.GREATER_THAN, OperandNames.timestamp.toString(), "100000");
        String query = queryGenerator.generateQuery(OperatorName.AND, subquery1, subquery2);


        String textAfterQuotesRemoval = "this is \" ) ( a valid text )";
        QueryTree<SetsOperator, Operand> expectedTree = QueryTree.<SetsOperator, Operand>builder()
                .operator(setsOperatorFactory.getOperator(OperatorName.AND))
                .left(QueryTree.<SetsOperator, Operand>builder()
                        .operator(setsOperatorFactory.getOperator(OperatorName.EQUAL))
                        .operand(operandFactory.createOperand(OperandNames.content, textAfterQuotesRemoval))
                        .build())
                .right(QueryTree.<SetsOperator, Operand>builder()
                        .operator(setsOperatorFactory.getOperator(OperatorName.GREATER_THAN))
                        .operand(operandFactory.createOperand(OperandNames.timestamp, "100000"))
                        .build())
                .build();

        QueryTree<SetsOperator, Operand> queryTree = parser.parse(query);
        assertEquals(expectedTree, queryTree);
    }

    @Test
    public void parseQueryWithNestedUnaryOperatorTest() throws ParsingException {
        String title = "\"wow\"";
        String subquery = queryGenerator.generateQuery(
                OperatorName.EQUAL, OperandNames.title.toString(), title);
        String query = queryGenerator.generateQuery(OperatorName.NOT, subquery);

        String unQuotedTitle = "wow";
        QueryTree expectedTree = QueryTree.builder()
                .operator(setsOperatorFactory.getOperator(OperatorName.NOT))
                .left(QueryTree.builder()
                        .operator(setsOperatorFactory.getOperator(OperatorName.EQUAL))
                        .operand(operandFactory.createOperand(OperandNames.title, unQuotedTitle))
                        .build())
                .build();

        QueryTree queryTree = parser.parse(query);

        assertEquals(expectedTree, queryTree);
    }

    @Test
    public void parseLongQueryTest() throws ParsingException {
        String subquery1 = queryGenerator.generateQuery(
                OperatorName.GREATER_THAN, OperandNames.views.toString(), "1000");
        String subquery2 = queryGenerator.generateQuery(
                OperatorName.LESS_THAN, OperandNames.views.toString(), "5000");
        String subquery3 = queryGenerator.generateQuery(OperatorName.AND, subquery1, subquery2);

        String subquery4 = queryGenerator.generateQuery(
                OperatorName.GREATER_THAN, OperandNames.timestamp.toString(), "1000000");
        String subquery5 = queryGenerator.generateQuery(OperatorName.NOT, subquery4);

        String query = queryGenerator.generateQuery(OperatorName.OR, subquery3, subquery5);


        QueryTree expectedTree = QueryTree.builder()
                .operator(setsOperatorFactory.getOperator(OperatorName.OR))
                .left(QueryTree.builder()
                        .operator(setsOperatorFactory.getOperator(OperatorName.AND))
                        .left(QueryTree.builder()
                                .operator(setsOperatorFactory.getOperator(OperatorName.GREATER_THAN))
                                .operand(operandFactory.createOperand(OperandNames.views, "1000"))
                                .build())
                        .right(QueryTree.builder()
                                .operator(setsOperatorFactory.getOperator(OperatorName.LESS_THAN))
                                .operand(operandFactory.createOperand(OperandNames.views, "5000"))
                                .build())
                        .build())
                .right(QueryTree.builder()
                        .operator(setsOperatorFactory.getOperator(OperatorName.NOT))
                        .left(QueryTree.builder()
                                .operator(setsOperatorFactory.getOperator(OperatorName.GREATER_THAN))
                                .operand(operandFactory.createOperand(OperandNames.timestamp, "1000000"))
                                .build())
                        .build())
                .build();

        QueryTree queryTree = parser.parse(query);

        assertEquals(expectedTree, queryTree);
    }
}