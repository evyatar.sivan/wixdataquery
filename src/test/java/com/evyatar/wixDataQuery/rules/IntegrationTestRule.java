package com.evyatar.wixDataQuery.rules;

import lombok.Getter;
import org.junit.rules.ExternalResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.stereotype.Component;

@Getter
@Component
public class IntegrationTestRule extends ExternalResource {
    @Autowired
    private TestRestTemplate restTemplate;
    @Value("${tests.schema}")
    private String schema;
    @Value("${server.port}")
    private int port;
    @Value("${tests.host}")
    private String host;
}
