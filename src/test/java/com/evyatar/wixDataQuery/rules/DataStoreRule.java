package com.evyatar.wixDataQuery.rules;

import com.evyatar.wixDataQuery.datastore.DataStore;
import com.evyatar.wixDataQuery.models.Data;
import lombok.Getter;
import org.junit.Before;
import org.junit.rules.ExternalResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Getter
@Component
public class DataStoreRule extends ExternalResource {

    @Autowired
    private DataStore<Data> datastore;

    private static final String ID_PERFIX = "id ";
    private static final String TITLE_PERFIX = "title: ";
    private static final String CONTENT_PERFIX = "content: ";
    private static final int VIEWS_FACTOR = 1000;

    public String idByI(int i) {
        return ID_PERFIX + i;
    }

    public String titleByI(int i) {
        return TITLE_PERFIX + i;
    }

    public String contentByI(int i) {
        return CONTENT_PERFIX + i;
    }

    public int viewsByI(int i) {
        return VIEWS_FACTOR * i;
    }

    public int timestampByI(int i) {
        return 1609842600 + 86400 * i;
    }

    @Before
    public void setUp() {
        IntStream.range(1, 10).forEach(i ->
                datastore.add(Data.builder()
                        .id(idByI(i))
                        .title(titleByI(i))
                        .content(contentByI(i))
                        .views(viewsByI(i))
                        .timestamp(timestampByI(i))
                        .build())
        );
    }
}
