package com.evyatar.wixDataQuery.integration.api;

import com.evyatar.wixDataQuery.controllers.DataQueryController;
import com.evyatar.wixDataQuery.models.Data;
import com.evyatar.wixDataQuery.rules.IntegrationTestRule;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
public class WixDataQueryApiTest {

    @Rule
    @Autowired
    public IntegrationTestRule rule;
    private UriComponentsBuilder uriComponentsBuilder;

    @Before
    public void setUrl() {
        uriComponentsBuilder = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host(rule.getHost())
                .port(rule.getPort())
                .path(DataQueryController.ENDPOINT);
    }

    @Test
    public void getRequestToEndpointTest() {
        String uri = uriComponentsBuilder
                .queryParam("query", "")
                .toUriString();
        ResponseEntity<Set> response = rule.getRestTemplate().getForEntity(uri, Set.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void getRequestToEndpointWithoutQueryParameterTest() {
        String uri = uriComponentsBuilder
                .toUriString();
        ResponseEntity<Object> response = rule.getRestTemplate().getForEntity(uri, Object.class);

        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void postRequestEmptyDataOkTest() {
        Data data = Data.builder()
                .build();
        String dataJson = new Gson().toJson(data);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(dataJson, headers);
        ResponseEntity<Data> response = rule.getRestTemplate().postForEntity(
                uriComponentsBuilder.toUriString(),
                request,
                Data.class
        );

        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }
}
