package com.evyatar.wixDataQuery.integration.query;

import com.evyatar.wixDataQuery.controllers.DataQueryController;
import com.evyatar.wixDataQuery.models.Data;
import com.evyatar.wixDataQuery.rules.DataStoreRule;
import com.evyatar.wixDataQuery.rules.IntegrationTestRule;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
public class PostDataTest {

    @Rule
    @Autowired
    public DataStoreRule dataStoreRule;
    @Autowired
    public IntegrationTestRule integrationTestRule;
    private UriComponentsBuilder uriComponentsBuilder;

    @Before
    public void setUrl() {
        uriComponentsBuilder = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host(integrationTestRule.getHost())
                .port(integrationTestRule.getPort())
                .path(DataQueryController.ENDPOINT);
    }

    @Test
    public void uploadDataOkTest() {
        String id = UUID.randomUUID().toString();
        Data data = Data.builder().
                id(id)
                .title("title")
                .build();
        String dataJson = new Gson().toJson(data);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(dataJson, headers);

        assertEquals(0, dataStoreRule.getDatastore().stream().filter(t -> id.equals(t.getId())).count());

        ResponseEntity<Data> response = integrationTestRule.getRestTemplate().postForEntity(
                uriComponentsBuilder.toUriString(),
                request,
                Data.class
        );

        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(1, dataStoreRule.getDatastore().stream().filter(t -> id.equals(t.getId())).count());
    }

    @Test
    public void reUploadDataTest() {
        String oldTitle = "oldTitle";
        String newTitle = "newTitle";
        String id = UUID.randomUUID().toString();

        Data data = Data.builder()
                .id(id)
                .title(oldTitle)
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String dataJson = new Gson().toJson(data);
        HttpEntity<String> request = new HttpEntity<String>(dataJson, headers);

        ResponseEntity<Data> response = integrationTestRule.getRestTemplate().postForEntity(
                uriComponentsBuilder.toUriString(),
                request,
                Data.class
        );

        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(1, dataStoreRule.getDatastore().stream().filter(t -> id.equals(t.getId())).count());
        assertEquals(0, dataStoreRule.getDatastore().stream()
                .filter(t -> id.equals(t.getId()) && newTitle.equals(t.getTitle())).count());

        data.setTitle(newTitle);

        dataJson = new Gson().toJson(data);
        request = new HttpEntity<String>(dataJson, headers);

        response = integrationTestRule.getRestTemplate().postForEntity(
                uriComponentsBuilder.toUriString(),
                request,
                Data.class
        );

        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(1, dataStoreRule.getDatastore().stream().filter(t -> id.equals(t.getId())).count());
        assertEquals(1, dataStoreRule.getDatastore().stream()
                .filter(t -> id.equals(t.getId()) && newTitle.equals(t.getTitle())).count());
    }
}