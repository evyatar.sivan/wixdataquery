package com.evyatar.wixDataQuery.integration.query;

import com.evyatar.wixDataQuery.controllers.DataQueryController;
import com.evyatar.wixDataQuery.logic.operands.Operand;
import com.evyatar.wixDataQuery.logic.operands.OperandFactory;
import com.evyatar.wixDataQuery.logic.operands.OperandNames;
import com.evyatar.wixDataQuery.logic.operators.OperatorName;
import com.evyatar.wixDataQuery.models.Data;
import com.evyatar.wixDataQuery.rules.DataStoreRule;
import com.evyatar.wixDataQuery.rules.IntegrationTestRule;
import com.evyatar.wixDataQuery.utils.queryGenerators.OperatorQueryGenerator;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
public class QueryingSanityTest {

    @Rule
    @Autowired
    public DataStoreRule dataStoreRule;
    @Autowired
    public IntegrationTestRule integrationTestRule;
    @Autowired
    private OperatorQueryGenerator<Operand> operatorQueryGenerator;
    @Autowired
    private OperandFactory operandFactory;
    private UriComponentsBuilder uriComponentsBuilder;

    @Before
    public void setUrl() {
        uriComponentsBuilder = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host(integrationTestRule.getHost())
                .port(integrationTestRule.getPort())
                .path(DataQueryController.ENDPOINT);
    }

    @Test
    public void emptyQueryReturnsAllTheEntitiesTest() {
        String query = "";
        Set<Data> expectedResult = dataStoreRule.getDatastore().stream()
                .collect(Collectors.toSet());

        executeTest(query, expectedResult);
    }

    @Test
    public void equalsIdReturnsTheRightItemTest() {
        String query = operatorQueryGenerator.generateQuery(
                OperatorName.EQUAL,
                operandFactory.createOperand(OperandNames.id, dataStoreRule.idByI(5))
        );
        Set<Data> expectedResult = dataStoreRule.getDatastore().stream()
                .filter(t -> dataStoreRule.idByI(5).equals(t.getId()))
                .collect(Collectors.toSet());

        executeTest(query, expectedResult);
    }

    @Test
    public void greaterThanViewsReturnsTheRightItemsTest() {
        String query = operatorQueryGenerator.generateQuery(
                OperatorName.GREATER_THAN,
                operandFactory.createOperand(OperandNames.views, String.format("%s", dataStoreRule.viewsByI(7)))
        );
        Set<Data> expectedResult = dataStoreRule.getDatastore().stream()
                .filter(t -> t.getViews() > dataStoreRule.viewsByI(7))
                .collect(Collectors.toSet());

        executeTest(query, expectedResult);
    }

    @Test
    public void lessThanTimestampReturnsTheRightItemsTest() {
        String query = operatorQueryGenerator.generateQuery(
                OperatorName.LESS_THAN,
                operandFactory.createOperand(OperandNames.timestamp, String.format("%s", dataStoreRule.timestampByI(4)))
        );
        Set<Data> expectedResult = dataStoreRule.getDatastore().stream()
                .filter(t -> t.getTimestamp() < dataStoreRule.timestampByI(4))
                .collect(Collectors.toSet());

        executeTest(query, expectedResult);
    }

    @Test
    public void andOperatorReturnsTheRightItemsTest() {
        String greaterThan3Query = operatorQueryGenerator.generateQuery(
                OperatorName.GREATER_THAN,
                operandFactory.createOperand(OperandNames.views, String.format("%s", dataStoreRule.viewsByI(3)))
        );
        String lessThan8Query = operatorQueryGenerator.generateQuery(
                OperatorName.LESS_THAN,
                operandFactory.createOperand(OperandNames.views, String.format("%s", dataStoreRule.viewsByI(8)))
        );
        String greaterThan3AndlessThan8Query = operatorQueryGenerator.generateQuery(
                OperatorName.AND, greaterThan3Query, lessThan8Query
        );
        Set<Data> expectedResult = dataStoreRule.getDatastore().stream()
                .filter(t -> {
                    return t.getViews() > dataStoreRule.viewsByI(3) &&
                            t.getViews() < dataStoreRule.viewsByI(8);
                })
                .collect(Collectors.toSet());

        executeTest(greaterThan3AndlessThan8Query, expectedResult);
    }

    @Test
    public void orOperatorReturnsTheRightItemsTest() {
        String greaterThan3Query = operatorQueryGenerator.generateQuery(
                OperatorName.LESS_THAN,
                operandFactory.createOperand(OperandNames.views, String.format("%s", dataStoreRule.viewsByI(3)))
        );
        String lessThan8Query = operatorQueryGenerator.generateQuery(
                OperatorName.GREATER_THAN,
                operandFactory.createOperand(OperandNames.views, String.format("%s", dataStoreRule.viewsByI(8)))
        );
        String greaterThan3AndlessThan8Query = operatorQueryGenerator.generateQuery(
                OperatorName.OR, greaterThan3Query, lessThan8Query
        );
        Set<Data> expectedResult = dataStoreRule.getDatastore().stream()
                .filter(t -> {
                    return t.getViews() < dataStoreRule.viewsByI(3) ||
                            t.getViews() > dataStoreRule.viewsByI(8);
                })
                .collect(Collectors.toSet());

        executeTest(greaterThan3AndlessThan8Query, expectedResult);
    }

    @Test
    public void notOperatorReturnsTheRightItemsTest() {
        String greaterThan3Query = operatorQueryGenerator.generateQuery(
                OperatorName.GREATER_THAN,
                operandFactory.createOperand(OperandNames.views, String.format("%s", dataStoreRule.viewsByI(3)))
        );

        String viewsNotGreaterThan3 = operatorQueryGenerator.generateQuery(
                OperatorName.NOT, greaterThan3Query
        );
        Set<Data> expectedResult = dataStoreRule.getDatastore().stream()
                .filter(t -> {
                    return !(t.getViews() > dataStoreRule.viewsByI(3));
                })
                .collect(Collectors.toSet());

        executeTest(viewsNotGreaterThan3, expectedResult);
    }

    private void executeTest(String query, Set<Data> expectedResult) {
        String uri = uriComponentsBuilder
                .queryParam("query", query)
                .toUriString();
        ResponseEntity<Set<Data>> response = integrationTestRule.getRestTemplate()
                .exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Set<Data>>() {
                });

        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertTrue(CollectionUtils.isEqualCollection(expectedResult, response.getBody()));
    }
}